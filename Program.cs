﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

using ALG_BLOOM_FILTER.Model;
using ALG_BLOOM_FILTER.Tools;

using Panwork;
using Panwork.Core.Data;

namespace ALG_BLOOM_FILTER {
    class Program {
        private static int linqSearch = 1000;
        private static int sqlSearch = 1000;
        private static Stopwatch stopWatch = new Stopwatch();
        private static List<Dictionary> _dictionary;
        private static BloomFilter filter;
        private static string[] words;
        private static string containedWords = "";
        private static bool printWords = false;

        private static IDatabase db = null;

        public static IDatabase Db {
            get {
                if(db == null) {
                    db = new MySqlDatabase("127.0.0.1", "dictionary", "root", "alg2018");
                    db.Query("Select 1");
                }

                return db;
            }
        }

        public static DataManager DataManager { get; set; }

        public static void Init() {
            Console.Write("Datenbank indizieren => ");

            stopWatch.Start();

            foreach (Dictionary dictionary in _dictionary) {
                filter.Insert(dictionary.Word);
            }

            stopWatch.Stop();
            Console.WriteLine("Optimale Anzahl an HashFunktionen: " + filter.HashFunctions);
            Console.WriteLine("Filtergröße: " + filter.Filter.Length + " bits = " + Math.Round(filter.Filter.Length / (8.0 * 1000), 2) + " Kilobyte");
            Console.WriteLine("Zeit einfügen von Datenbank in den BloomFilter: " + stopWatch.ElapsedMilliseconds + "ms");

            Console.WriteLine("Wörter aus Textdatei lesen...");
            words = Program.Read("words.txt");
            Console.WriteLine();
        }

        public static void FpBloomFilter() {
            containedWords = "";
            Console.Write("BloomFilter mit FalsePositive Ergebnissen für " + words.Length + " Elemente bei " + _dictionary.Count() + " Datenbank Elementen => ");
            stopWatch.Reset();
            stopWatch.Start();
            int i = 0;
            foreach (string word in words) {
                if (filter.Contains(word)) {
                    i++;
                    if (printWords) {
                        containedWords += word + System.Environment.NewLine;
                    }
                }
            }
            stopWatch.Stop();
            Console.WriteLine("Ergebnisse, die Enthalten sind: " + i + ", Zeit beim Bloom Filter: " + stopWatch.ElapsedMilliseconds + "ms");
            if (printWords) {
                Console.WriteLine(containedWords);
            }
        }

        public static void LinqBloomFilter() {
            containedWords = "";
            Console.Write("BloomFilter mit LinqPrüfung Suche für " + words.Length + " Elemente bei " + _dictionary.Count() + " Datenbank Elementen => ");
            stopWatch.Reset();
            stopWatch.Start();
            int i = 0;
            foreach (string word in words) {
                if (filter.Contains(word)) {
                    if (_dictionary.Where(d => d.Word == word).Any()) {
                        i++;
                        if (printWords) {
                            containedWords += word + System.Environment.NewLine;
                        }
                    }
                }
            }
            stopWatch.Stop();
            Console.WriteLine("Ergebnisse, die Enthalten sind: " + i + ", Zeit bei Linq und Bloom Filter: " + stopWatch.ElapsedMilliseconds + "ms");
            if (printWords) {
                Console.WriteLine(containedWords);
            }
        }

        public static void LinqSearch() {
            Console.Write("Linq Suche für " + linqSearch + " Elemente bei " + _dictionary.Count() + " Datenbank Elementen => ");
            stopWatch.Reset();
            stopWatch.Start();
            int i = 0;
            for (int j = 0; j < 1000; j++) {
                if (_dictionary.Where(d => d.Word == words[j]).Any()) {
                    i++;
                }
            }
            stopWatch.Stop();
            Console.WriteLine("Ergebnisse, die bei Linq Enthalten sind: " + i + ", Zeit bei Linq: " + stopWatch.ElapsedMilliseconds + "ms");
        }

        public static void SqlSearch() {
            Console.Write("SQL Suche für " + sqlSearch + " Elemente bei " + _dictionary.Count() + " Datenbank Elementen => ");
            stopWatch.Reset();
            stopWatch.Start();
            int i = 0;
            for (int j = 0; j < sqlSearch; j++) {
                Program.Db.Query("SELECT * FROM dictionary WHERE word LIKE 'Apple'");
            }
            stopWatch.Stop();
            Console.WriteLine("Zeit bei SQL: " + stopWatch.ElapsedMilliseconds + "ms");
        }

        public static void TestBloomFilter(int m, long elements) {
            if (!Test.Tests.Where(t => t.M == m).Any()) {
                Test test = Test.Create(m);
                BloomFilter filter = new BloomFilter(m, elements);

                for (int i = 0; i < elements; i++) {
                   
                    int j = 0;

                    filter.Insert(_dictionary[i].Word);

                    if (i == 10 || i == 100 || i == 1000 || i == 10000 || i == 100000) {
                        foreach (string word in words) {
                            if (filter.Contains(word)) {
                                j++;
                            }
                        }

                        TestData.Create(test, i , Math.Round(Math.Pow(0.6185, m * 1.0 / i), 2), Math.Round((j * 1.0 / words.Length), 2));
                    }

                   
                }
            }
        }

        public static string[] Read(string name) => Panwork.Tools.FileTools.Read(name).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

        static void Main(string[] args) {
            Console.WriteLine("Beispiel Hashfunktionen testen   [1]");
            Console.WriteLine("BloomFilter ausführen            [2]");
            Console.WriteLine();
            Console.Write("Eingabe: ");

            int insert = int.Parse(Console.ReadLine());
            if(insert == 1) {
                TestHash(new string[] { "Jan" , "Johannes" , "Leon" , "Sven" , "Daniel" , "Max" , "Moritz" , "Mathias" , "Martin"}, 10);
                Console.ReadKey();

            } else {
                BloomFilter();
            }
        }

        public static void TestHash(string[] items, int length) {
            BitArray array = new BitArray(length);
            foreach(string item in items) {
                BigInteger hash1 = Hash1(item);
                array[(int)(hash1 % length)] = true;
                Console.Write("1. HashFunktion für " + item + ": " + hash1 + " Länge des Arrays: " + length + " Hash mit Modulo: " + (hash1 % length));
                Console.WriteLine();
                BigInteger hash2 = Hash2(item);
                array[(int)(hash2 % length)] = true;
                Console.Write("2. HashFunktion für " + item + ": " + hash2 + " Länge des Arrays: " + length + " Hash mit Modulo: " + (hash2 % length));
                Console.WriteLine();

                Console.WriteLine("Bitarray: ");
                Console.Write("Index:   ");
                for(int i = 0; i < length; i++) {
                    Console.Write(i + " ");
                }

                Console.WriteLine();

                Console.Write("Bits:    ");
                foreach (var bit in array) {
                    Console.Write(((bool)bit ? "1" : "0") + " ");
                }

                Console.WriteLine();
            }
        }

        public static BigInteger Hash1(string item) {
            BigInteger ret = 0;
            Console.Write("Hash1: ");

            for(int i = 0; i < item.Length; i++) {
                if(i < item.Length - 1) {
                    Console.Write((int)item[i] + " + ");
                } else {
                    Console.Write((int)item[i]);
                }
          
                ret += (int)item[i];
            }

            Console.Write(" = " + ret);
            Console.WriteLine();
            return ret;
        }

        public static BigInteger Hash2(string item) {
            BigInteger ret = 0;
            Console.Write("Hash2: ");
            for(int i = 0; i < item.Length - 1; i++) {
                Console.Write("(");
            }

            Console.Write("(" + ret);

            for (int i = 0; i < item.Length; i++) {
                Console.Write(" + " + (int)item[i] + " ) ");
                ret += (int)item[i];
                Console.Write(" * " + (int)item[i]);
                ret *= (int)item[i];
            }

            Console.Write(" = " + ret);
            Console.WriteLine();

            return ret;
        }

        public static void BloomFilter() {

            try {
                Program.DataManager = new DataManager("BLOOM", Program.Db, Globals.Logger);
                Globals.DataManager = Program.DataManager;

                Console.Write("Geben Sie die Wahrscheinlichkeit eines FalsePositive Ergebnisses ein: ");
                double fpRate = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

                _dictionary = Dictionary.Words;
                filter = new BloomFilter(_dictionary.Count(), fpRate);

                Init();

                int key = 0;
                do {
                    Console.WriteLine("Neue Wahrscheinlichkeit eingeben             [1]");
                    Console.WriteLine("Bloom Filter mit FalsePositive Ergebnissen   [2]");
                    Console.WriteLine("Laufzeit Linq Abfrage                        [3]");
                    Console.WriteLine("Bloom Filter mit Linq überprüfung            [4]");
                    Console.WriteLine("Laufzeit Sql Abfrage                         [5]");
                    Console.WriteLine("Bloom Filter Dauer Einzelwert                [6]");
                    Console.WriteLine("Linq Dauer Einzelwert                        [7]");
                    Console.WriteLine("SQL dauer Einzelwert                         [8]");
                    Console.WriteLine("Teste Bloom Filter                           [9]");
                    Console.WriteLine("Beenden                                      [10]");
                    Console.WriteLine();
                    Console.Write("Eingabe: ");
                    key = int.Parse(Console.ReadLine());
                    Console.WriteLine();

                    switch (key) {
                        case 1:
                            Console.Write("Geben Sie die Wahrscheinlichkeit eines FalsePositive Ergebnisses ein: ");
                            fpRate = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
                            filter = new BloomFilter(_dictionary.Count(), fpRate);
                            Init();
                            break;
                        case 2:
                            FpBloomFilter();
                            break;
                        case 3:
                            LinqSearch();
                            break;
                        case 4:
                            LinqBloomFilter();
                            break;
                        case 5:
                            SqlSearch();
                            break;
                        case 6:
                            stopWatch.Reset();
                            stopWatch.Start();
                            filter.Contains("Zythum");
                            stopWatch.Stop();
                            Console.WriteLine("Dauer BloomFilter Suche: " + "Zeit in ms: " + stopWatch.ElapsedMilliseconds + " Zeit in Ticks: " + stopWatch.ElapsedTicks);
                            break;
                        case 7:
                            stopWatch.Reset();
                            stopWatch.Start();
                            _dictionary.Where(d => d.Word == words[linqSearch]).Any();
                            stopWatch.Stop();
                            Console.WriteLine("Dauer Linq Suche: " + "Zeit in ms: " + stopWatch.ElapsedMilliseconds + " Zeit in Ticks: " + stopWatch.ElapsedTicks);
                            break;
                        case 8:
                            stopWatch.Reset();
                            stopWatch.Start();
                            Program.Db.Query("SELECT * FROM dictionary WHERE word LIKE 'Zythum'");
                            stopWatch.Stop();
                            Console.WriteLine("Dauer SQL Suche: " + "Zeit in ms: " + stopWatch.ElapsedMilliseconds + " Zeit in Ticks: " + stopWatch.ElapsedTicks);
                            break;
                        case 9:
                            TestBloomFilter(512, _dictionary.Count());
                            TestBloomFilter(4096, _dictionary.Count());
                            TestBloomFilter(65536, _dictionary.Count());
                            TestBloomFilter(131072, _dictionary.Count());
                            break;
                        case 10:
                            break;
                        default:
                            key = 0;
                            break;
                    }

                    Console.WriteLine();
                } while (key < 10);
            }
            catch (Exception ex) {
                Console.WriteLine(ex);
                Console.ReadKey();
            }
        }
    }
}
