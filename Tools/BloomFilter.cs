﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ALG_BLOOM_FILTER.Tools {
    public class BloomFilter {

        public int HashFunctions { get; set; }

        public BitArray Filter { get; set; }

        public BloomFilter() => this.Init(1000, 0.1);

        public void Init(int filterLength, long elements) {
            this.Filter = new BitArray(filterLength);
            this.HashFunctions = (int)Math.Ceiling((filterLength * 1.0 / elements) * Math.Log(2));
        }

        public void Init(long elements, double fpRate) {
            int bits = (int)Math.Ceiling((-elements) * Math.Log(fpRate) / (Math.Pow(Math.Log(2), 2)));
            this.Filter = new BitArray(bits);
            this.HashFunctions = (int)Math.Ceiling((bits * 1.0 / elements) * Math.Log(2));
        }

        public BloomFilter(long elements, double fpRate) => this.Init(elements, fpRate);

        public BloomFilter(int filterLength, long elements) => this.Init(filterLength, elements);

        public void Insert(string str) {
            for (int i = 0; i < this.HashFunctions; i++) {
                this.Filter[(int)(this.HashFunction(str, i) % this.Filter.Length)] = true;
            }
        }

        public bool Contains(string str) {
            bool ret = true;

            for (int i = 0; i < this.HashFunctions; i++) {
                if (this.Filter[(int)(this.HashFunction(str, i) % this.Filter.Length)] == false) {
                    ret = false;
                    break;
                }
            }

            return ret;
        }

        public BigInteger HashFunction(string str, int k) {
            var h1 = BloomFilter.AddBit(System.Data.HashFunction.CRC.
                CRCFactory.Instance.Create().ComputeHash(Encoding.ASCII.GetBytes(str)).Hash.ToArray());
            var h2 = BloomFilter.AddBit(System.Data.HashFunction.MurmurHash.MurmurHash3Factory.Instance.Create().ComputeHash(Encoding.ASCII.GetBytes(str)).Hash.ToArray());
            return new BigInteger(h1) + k * new BigInteger(h2) + ((BigInteger)Math.Pow(k, 2));
        }

        public static byte[] AddBit(byte[] array) {
            byte[] ret = new byte[array.Length + 1];
            for (int i = 0; i < array.Length; i++) {
                ret[i] = array[i];
            }

            ret[array.Length] = 0x00;

            return ret;
        }
    }
}
