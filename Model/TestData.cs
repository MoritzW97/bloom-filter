﻿using Panwork.Core.Data;


namespace ALG_BLOOM_FILTER.Model {

    [DbTable(Name = "testdata")]
    public class TestData : PersistentObject {

        public static TestData Create(Test test, int n, double fpRate, double fp) {
            TestData data = Program.DataManager.Create<TestData>();
            data.Test = test.Id;
            data.N = n;
            data.FpRate = fpRate;
            data.Fp = fp;
            data.Save();

            return data;
        }

        [DbColumn(IsPrimaryKey = true, IsGeneratedKey = true, ReadOnly = true)]
        public long Id { get; set; }

        [DbColumn]
        public long Test { get; set; }

        [DbColumn]
        public int N { get; set; }

        [DbColumn]
        public double FpRate { get; set; }

        [DbColumn]
        public double Fp { get; set; }

    }
}
