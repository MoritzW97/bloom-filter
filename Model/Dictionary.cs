﻿using System.Collections.Generic;

using Panwork.Core.Data;


namespace ALG_BLOOM_FILTER.Model {

    [DbTable(Name = "dictionary")]
    public class Dictionary : PersistentObject {

        public static Dictionary Create(string word) => Dictionary.Create(word, "");

        public static Dictionary Create(string word, string wordType) => Dictionary.Create(word, wordType, "");

        public static Dictionary Create(string word, string wordType, string definition) {
            Dictionary dictionary = Program.DataManager.Create<Dictionary>();
            dictionary.Word = word;
            dictionary.Save();
            return dictionary;
        }

        [DbColumn(IsPrimaryKey = true, IsGeneratedKey = true, ReadOnly = true)]
        public long Id { get; set; }

        [DbColumn]
        public string Word { get; set; }

        [DbColumn]
        public string WordType { get; set; }

        [DbColumn]
        public string Definition { get; set; }

        public static Dictionary Default => Program.DataManager.Get<Dictionary>(1);

        public static List<Dictionary> Words => Program.DataManager.GetAll<Dictionary>();

        public override string ToString() => this.Word;

    }
}
