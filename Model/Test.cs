﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Panwork.Core.Data;

namespace ALG_BLOOM_FILTER.Model {

    [DbTable(Name = "test")]
    public class Test : PersistentObject {
        public static Test Create(int m) {
            Test test = Program.DataManager.Create<Test>();
            test.M = m;
            test.Save();

            return test;
        }

        [DbColumn(IsPrimaryKey = true, IsGeneratedKey = true, ReadOnly = true)]
        public long Id { get; set; }

        [DbColumn]
        public int M { get; set; }

        public static List<Test> Tests => Program.DataManager.GetAll<Test>();
    }
}
